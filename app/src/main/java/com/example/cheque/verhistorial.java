package com.example.cheque;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

public class verhistorial extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verhistorial);

        activarBotones();

        Button btVolver = (Button) findViewById(R.id.btVolver);
        btVolver.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

    }

    private void activarBotones() {

        FloatingActionButton btInfo1 = (FloatingActionButton) findViewById(R.id.btInfo1);
        btInfo1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                accionBotonInfo();
            }
        });

        FloatingActionButton btInfo2 = (FloatingActionButton) findViewById(R.id.btInfo2);
        btInfo2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                accionBotonInfo2();
            }
        });

    }

    private void accionBotonInfo() {
        Intent infocheque = new Intent(this, com.example.cheque.infocheque.class);
        startActivity(infocheque);
    }

    private void accionBotonInfo2() {
        Intent infocheque2 = new Intent(this, com.example.cheque.infocheque2.class);
        startActivity(infocheque2);
    }
}